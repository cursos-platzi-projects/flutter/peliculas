import 'package:flutter/material.dart';
import 'package:peliculas/providers/movies_provider.dart';
import 'package:peliculas/search/search_delegate.dart';
import 'package:peliculas/widgets/widgets.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {


  @override
  Widget build(BuildContext context) {

    //Despues del context hay un segundo parametro que es 'listen' y ayuda a renderizar en automatico o no cuando cambian los datos, por default esta en true
    //En algunas partes se usa el false ya que hay algunos widgets que no se pueden re-dibujar y marcaria error si esta en true
    final moviesProvider = Provider.of<MoviesProvider>(context);

    print( moviesProvider.onDisplayMovies );

    return Scaffold(
      appBar: AppBar(
        title: Text('Peliculas en cines'),
        elevation: 0,
        actions: [
          IconButton(
            icon: Icon( Icons.search_outlined ),
            //delegate = es una clase que tiene ciertas condiciones a crear
            onPressed: () => showSearch(context: context, delegate: MovieSearchDelegate()),
          )
        ],
      ),
      // SingleChildScrollView = para que pueda alojar N cantidad de Widgets y pueda hacer scrol en la pagina
      body: SingleChildScrollView(
        child: Column(
          children: [

            //Tarjetas principales
            CardSwiper( movies: moviesProvider.onDisplayMovies ),

            //Slider de peliculas
            MovieSlider(
              movies: moviesProvider.popularMovies,//populares
              title: 'Peliculas populares', //opcional
              onNextPage: () => moviesProvider.getPopularMovies(),
            ),


          ],
        ),
      )
    );
  }
}

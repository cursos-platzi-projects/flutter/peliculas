import 'package:flutter/material.dart';
import 'package:peliculas/models/models.dart';
import 'package:peliculas/widgets/widgets.dart';

class DetailsScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    //TODO: Cambiar luego por una instancia de movie
    //Esto es para obtener los parametros de la ruta
    final Movie movie = ModalRoute.of(context)!.settings.arguments as Movie;

    return Scaffold(
      //CustomScrollView = es similar al 'SingleChildScrollView' widget con la diferencia de que este tiene slivers
      //slivers = son widget pre-programados que van cambiando cuando se le hace scroll a la pagina
      body: CustomScrollView(
        slivers: [
          _CustomAppBar( movie ),
          //SliverList = con este widget se puede insertar widgets normales
          SliverList(
              delegate: SliverChildListDelegate([
                _PosterAndTitle( movie ),

                _Overview( movie.overview ),

                CastingCards( movie.id ),
              ])
          )
        ],
      )
    );
  }
}

class _CustomAppBar extends StatelessWidget {

  final Movie movie;

  const _CustomAppBar(this.movie);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Colors.indigo,
      expandedHeight: 200,
      floating: false,
      pinned: true, //Para que no desaparesca cuando se haga scroll
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        titlePadding: const EdgeInsets.all(0), //Se come el padding que tenia abajo de movie.title
        title: Container(
          width: double.infinity,
          alignment: Alignment.bottomCenter,
          padding: EdgeInsets.only( bottom: 10, left: 10, right: 10),
          color: Colors.black12,
          child: Text(
            movie.title,
            style: TextStyle( fontSize: 16 ),
            textAlign: TextAlign.center,
          ),
        ),
        background: FadeInImage(
          placeholder: AssetImage('lib/assets/loading.gif'),
          image: NetworkImage( movie.fullBackdropPath ),
          fit: BoxFit.cover, // Para que se expanda sin perder los bordes
        ),
      ),
    );
  }
}

class _PosterAndTitle extends StatelessWidget {

  final Movie movie;

  const _PosterAndTitle(this.movie);

  @override
  Widget build(BuildContext context) {
    //Esta linea para no repetir codigo
    final textTheme = Theme.of(context).textTheme;
    final size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.only( top: 20 ),
      padding: EdgeInsets.symmetric( horizontal: 20 ),
      child: Row(
        children: [
          //Hero = Es una animacion que le da cuando selecciona la movie
          //Tiene que estar ligado en ambos lados para que funcione como una animacio (inicio-final)
          Hero(
            tag: movie.heroId!,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: FadeInImage(
                placeholder: AssetImage('lib/assets/loading.gif'),
                image: NetworkImage(movie.fullPosterImg),
                height: 150,
              ),
            ),
          ),

          SizedBox( width: 20,),

          //Se tuvo que poner el 'ConstrainedBox' por problemas visuales que tenia el titulo y el titulo-original
          ConstrainedBox(
            constraints: BoxConstraints( maxWidth: size.width - 190),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Text(movie.title, style: textTheme.headline5, overflow: TextOverflow.ellipsis, maxLines: 1,),

                Text(movie.originalTitle, style: textTheme.subtitle1, overflow: TextOverflow.ellipsis, maxLines: 2,),

                Row(
                  children: [
                    Icon( Icons.star_outline, size: 15, color: Colors.grey),
                    SizedBox( width: 5,),
                    Text('${movie.voteAverage}', style: textTheme.caption,)
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _Overview extends StatelessWidget {

  final String overview;

  const _Overview(this.overview);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric( horizontal: 30, vertical: 10 ),
      child: Text(
        overview,
        textAlign: TextAlign.justify,
        style: Theme.of(context).textTheme.subtitle1,
      ),
    );
  }
}

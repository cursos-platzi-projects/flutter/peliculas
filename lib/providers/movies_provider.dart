import 'dart:async';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:peliculas/helpers/debouncer.dart';
import 'package:peliculas/models/models.dart';
import 'package:peliculas/models/search_response.dart';

class MoviesProvider extends ChangeNotifier {

  String _apiKey = '8552364b55b61be93122f4d049e54e93';
  String _baseUrl = 'api.themoviedb.org';
  String _language = 'es-ES';

  List<Movie> onDisplayMovies = [];
  List<Movie> popularMovies = [];

  //Variable para los actores
  Map<int, List<Cast>> moviesCast = {};

  int _popularPage = 0;

  final debouncer = Debouncer(
      duration: Duration(milliseconds: 500),
      //onValue: () //Cuando se dispare
  );

  final StreamController<List<Movie>> _suggestionStreamController = new StreamController.broadcast();
  Stream<List<Movie>> get suggestionStream => this._suggestionStreamController.stream;

  MoviesProvider() {
    print('MoviesProvider inicializado');

    //Se le puede omitir el this pero siempre es bueno manejarlo con el this
    this.getOnDisplayMovies();
    this.getPopularMovies();
  }


  getOnDisplayMovies() async {
    final jsonData = await _getJsonData('3/movie/now_playing');
    final nowPlayingResponse = NowPlayingResponse.fromJson(jsonData);

    //print( nowPlayingResponse.results[1].title );
    onDisplayMovies = nowPlayingResponse.results;

    //Esto es para que lo widgets escuchen si hay cambios en la data para que se renderizen en automatico
    notifyListeners();

  }

  getPopularMovies() async {

    _popularPage++;

    final jsonData = await _getJsonData("3/movie/popular", _popularPage);
    final popularResponse = PopularResponse.fromJson(jsonData);

    //Se destructura ya que como vamos a ir cambiando de pagina, siempre queremos obtener las peliculas anteriores y con esto de la destructuracion se pueden mantener
    popularMovies = [ ...popularMovies, ...popularResponse.results ];

    //Esto es para que lo widgets escuchen si hay cambios en la data para que se renderizen en automatico
    notifyListeners();
  }

  //[int page = 1] para que sea opcional, si el valor no viene por defecto es 1
  Future<String> _getJsonData( String endpoint, [int page = 1] ) async {
    final url = Uri.https( _baseUrl , endpoint, {
      'api_key': _apiKey,
      'language': _language,
      'page': '$page'
    });

    final response = await http.get(url);
    return response.body;
  }

  Future<List<Cast>> getMovieCast( int movieId ) async {
    /**
     * Esta condicion sirve para no volver a cargar la informacion si ya la tenemos anteriormente, ayuda al performance
     *
     * En moviesCast[movieId] se le pone "!" ya que dart detecta que puede venir valores vacios y te da error si no lo ponemos
     * pero como sabemos que siempre va a venir, solo se le pone para que no nos de error el programa
     * */
    if( moviesCast.containsKey(movieId) ) return moviesCast[movieId]!;

    final jsonData = await _getJsonData('3/movie/$movieId/credits');
    final creditsResponse = CreditsResponse.fromJson( jsonData );

    moviesCast[movieId] = creditsResponse.cast;

    return creditsResponse.cast;
  }

  Future<List<Movie>> searchMovies( String query ) async {
    final url = Uri.https( _baseUrl , '3/search/movie', {
      'api_key': _apiKey,
      'language': _language,
      'query': query
    });

    final response = await http.get(url);
    final searchResponse = SearchResponse.fromJson( response.body );

    return searchResponse.results;
  }

  //Metodo para cuando la persona deje de escribir en la busqueda
  void getSuggestionByQuery( String searchTerm ) {
    debouncer.value = '';
    //Esto se ejecuta despues de que pasen los 500 milisegundos
    debouncer.onValue = (value) async {
      //print('Tenemos el valor a buscar $value');
      final results = await this.searchMovies(value);
      this._suggestionStreamController.add(results);
    };

    final timer = Timer.periodic(Duration(milliseconds: 300), (_) {
      debouncer.value = searchTerm;
    });

    Future.delayed(Duration(milliseconds: 301)).then(( _ ) => timer.cancel());
  }

}
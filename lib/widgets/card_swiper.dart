import 'package:flutter/material.dart';
import 'package:flutter_card_swipper/flutter_card_swiper.dart';
import 'package:peliculas/models/models.dart';

class CardSwiper extends StatelessWidget {

  final List<Movie> movies;

  const CardSwiper({
    Key? key,
    required this.movies
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    //Para obtener las dimenciones de la pantalla
    final size = MediaQuery.of(context).size;

    //Esta condicion es para que muestre un loading cuando no hay datos aun por mostrar y se muestre un loading de que esta cargando los datos
    if( this.movies.length == 0 ) {
      return Container(
        width: double.infinity,
        height: size.height * 0.5,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    return Container(
      width: double.infinity, //Para que tome toddo el ancho de la pantalla
      height: size.height * 0.5, //Para que rellene el 50% de la pantalla
      child: Swiper(
        itemCount: movies.length,
        layout: SwiperLayout.STACK,
        itemWidth: size.width * 0.6,
        itemHeight: size.height * 0.6,
        //Si no se usa el BuildContext entonces se puede poner un "_"
        //Cuando se vea un Builder, significa que va a renderizar algo
        itemBuilder: ( _ , int index ) {

          final movie = movies[index];

          movie.heroId = 'swiper-${movie.id}';

          return GestureDetector(
            onTap: () => Navigator.pushNamed(context, 'details', arguments: movie),
            //Hero = Es una animacion que le da cuando selecciona la movie
            child: Hero(
              tag: movie.heroId!,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: FadeInImage(
                  placeholder: AssetImage('lib/assets/no-image.jpg'),
                  image: NetworkImage( movie.fullPosterImg ),
                  fit: BoxFit.cover, //Para adaptar la imagen al contenedor en el que esta
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
